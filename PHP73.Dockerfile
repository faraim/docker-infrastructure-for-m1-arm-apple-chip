FROM php:7.3-fpm
RUN docker-php-ext-install mysqli
RUN apt-get update && \
    apt-get install -y libxml2-dev
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
RUN docker-php-ext-install soap
RUN pecl install xdebug && docker-php-ext-enable xdebug